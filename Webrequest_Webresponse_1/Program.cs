﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Webrequest_Webresponse_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string website_address = "http://studynildana.com";
            //lets call the method that will do a webrequest and webresponse
            lets_call_web_request_response(website_address);

            //lets call the method that will do webrequest and webresponse - the async version
            var something = lets_call_web_request_response_async(website_address);

            Console.WriteLine("That's all folks!");
            Console.WriteLine("If we stick to this course, WE WILL DIE! WE WILL ALL DIE!!!");
            Console.ReadLine();
        }

        private static async Task lets_call_web_request_response_async(string website_address)
        {
            //building the web request object
            WebRequest address_to_request = WebRequest.Create(website_address);
            //collecting the response via an async method
            WebResponse collecting_the_response = await address_to_request.GetResponseAsync();

            //we got the response now. lets put it in a stream
            StreamReader response_to_stream_object = new StreamReader(collecting_the_response.GetResponseStream());

            //now turning the stream into text
            string text_of_web_response = await response_to_stream_object.ReadToEndAsync();

            //closing the response object
            collecting_the_response.Close();

            Console.WriteLine("---------------ASYNC WEB RESPONSE STARTS HERE-------------------");
            Console.WriteLine(text_of_web_response);
            Console.WriteLine("---------------ASYNC WEB RESPONSE ENDS HERE---------------------");
        }

        //this method will do the standard web request and web response
        private static void lets_call_web_request_response(string website_address)
        {
            //building the web request with the address provided
            WebRequest address_to_request = WebRequest.Create(website_address);
            //executing the request and collecting the response
            WebResponse collecting_the_response = address_to_request.GetResponse();

            //we are always working with streams. so using a stream object to read the response from the website
            StreamReader response_to_stream_object = new StreamReader(collecting_the_response.GetResponseStream());

            //now converting the stream data into text data
            string text_of_web_response = response_to_stream_object.ReadToEnd();

            //closing the response object
            //of course, even if you dont close the app should still run
            //still, this is better
            collecting_the_response.Close();

            Console.WriteLine("---------------WEB RESPONSE STARTS HERE-------------------");
            Console.WriteLine(text_of_web_response);
            Console.WriteLine("---------------WEB RESPONSE ENDS HERE---------------------");
        }
    }
}















